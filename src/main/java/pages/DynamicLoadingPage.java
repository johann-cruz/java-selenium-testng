package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class DynamicLoadingPage {
    WebDriver driver;
    private String linkXpath_Format = ".//a[contains(text(), '%s')";
    private By link_Example1 = By.xpath("//a[normalize-space()='Example 1: Element on page that is hidden']");
    private By link_Example2 = By.xpath("//a[normalize-space()='Example 2: Element rendered after the fact']");

    public DynamicLoadingPage(WebDriver driver){
        this.driver = driver;
    }

    public DynamicLoadingExample1Page clickExample1(){
        driver.findElement(link_Example1).click();
        return new DynamicLoadingExample1Page(driver);
    }
}
