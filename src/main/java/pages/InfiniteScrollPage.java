package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class InfiniteScrollPage {
    WebDriver driver;
    private By textBlocks = By.id("iscroll-added");

    public InfiniteScrollPage(WebDriver driver){
        this.driver = driver;
    }

    /**
     * Scrolls until paragraph with index specified is in view
     * @param index 1-based
     */
    public void scrollToParagraph(int index){
        String script = "window.scrollTo(0, document.body.scrollHeight)";
        var jsExecutor = (JavascriptExecutor) driver;

        while (getnumberOfParagraphsPresent() < index){
            jsExecutor.executeScript(script);
        }
    }

    public int getnumberOfParagraphsPresent(){
        return 5;
        //return driver.findElements(textBlocks).size();
    }
}
