package base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import pages.HomePage;
import utils.WindowManager;
import java.net.MalformedURLException;
import java.net.URL;

public class BaseTests {

    protected WebDriver driver;
    protected HomePage homePage;

    @Parameters({"Port"})
    @BeforeMethod
    public void initiateDriver(String Port) throws MalformedURLException {
        String node = "http:localhost:4444/wd/hub";
        if(Port.equalsIgnoreCase("9001")){
            driver = new RemoteWebDriver(new URL(node), new ChromeOptions());
            driver.manage().window().maximize();
        }
        else if(Port.equalsIgnoreCase("9002")){
            driver = new RemoteWebDriver(new URL(node), new FirefoxOptions());
            driver.manage().window().maximize();
        }
        else if(Port.equalsIgnoreCase("9003")){
            driver = new RemoteWebDriver(new URL(node), new FirefoxOptions());
            driver.manage().window().maximize();
        }

        driver.get("https://the-internet.herokuapp.com/");
        homePage = new HomePage(driver);
    }

    @AfterClass
    public void closeDriver()
    {
        driver.quit();
    }

    public WindowManager getWindowManager(){
        return new WindowManager(driver);
    }
}